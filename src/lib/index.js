/** @flow */
import Search from './Search'
import SearchIndex from './SearchIndex'
import SearchWorkerLoader from './worker/Main'

export default SearchWorkerLoader
export { Search, SearchIndex, SearchWorkerLoader }
